import React, { useContext } from 'react'
import { UserContext } from '../context/userContext'
import { AuthContext } from '../reducers/AuthContext'
import { types } from '../types/types'

const Navbar = () => {
  const { name } = JSON.parse(localStorage.getItem('user'))
  const { setisloggedIn } = useContext(UserContext)
  const { dispatch } = useContext(AuthContext)

  const handleLogOut = () => {
    // setisloggedIn(false);
    // localStorage.clear();
    const action = {
      type: types.logout
    }
    dispatch(action)
  }
  return (
    <div className='navbar navbar-max-container fixed-top'>
      {name && name !== '' && (
        <div className='welcome-name'>
          <p>
            Bienvenido <span>{name}</span>
          </p>
        </div>
      )}
      <div className='logout'>
        <button className='button-danger' onClick={handleLogOut}>
          Cerrar Sesion
        </button>
      </div>
    </div>
  )
}

export default Navbar
