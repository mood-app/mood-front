import React, { useContext } from 'react'
import 'react-responsive-modal/styles.css'
import { Modal } from 'react-responsive-modal'
import { ModalContext } from '../context/modalContext'
import { Link, useHistory } from 'react-router-dom'

const ModalEtapa1 = () => {
  let history = useHistory()
  const { modalEtapa1, setmodalEtapa1 } = useContext(ModalContext)
  console.log(modalEtapa1)

  const handleContinueModal = () => {
    history.push('etapaIdentificacion01')
    setmodalEtapa1(false)
  }
  return (
    <Modal
      open={modalEtapa1}
      onClose={() => {
        setmodalEtapa1(false)
      }}
      center
      classNames={{
        overlay: 'customOverlay',
        modal: 'customModal'
      }}
      closeOnEsc={true}
      showCloseIcon={true}
      closeOnOverlayClick={true}
      blockScroll={true}
    >
      <div className='modal-etapaident'>
        <h2>ETAPA DE IDENTIFICACIÓN</h2>

        <p>
          TE INVITO A QUE IDENTIFIQUES EL FACTOR DE RIESGO OPERATIVO, EL TIPO DE
          EVENTO DE RIESGO OPERATIVO Y LAS CAUSAS QUE LLEVARON A QUE ESTE EVENTO
          DE RIESGO SE MATERIALIZARA EN SEGURO FÁCIL.
        </p>
        <div className='button-next'>
          <img src='img/mascota/buho.png' alt='' />

          <button className='button-primary' onClick={handleContinueModal}>
            Continuar
          </button>
        </div>
      </div>
    </Modal>
  )
}

export default ModalEtapa1
