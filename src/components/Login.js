import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import validator from 'validator'
import { useAlert } from 'react-alert'

import axios from 'axios'
import { UntilContext } from '../context/untilContext'
import { UserContext } from '../context/userContext'
import { AuthContext } from '../reducers/AuthContext'
import { types } from '../types/types'
const Login = () => {
  const { dispatch } = useContext(AuthContext)

  const alert = useAlert()
  const [viewPassword, setviewPassword] = useState(false)
  const [dataLoginForm, setdataLoginForm] = useState({
    email: '',
    password: ''
  })
  const { host } = useContext(UntilContext)
  const { setisloggedIn } = useContext(UserContext)

  const { email, password } = dataLoginForm

  const onchangeForm = e => {
    setdataLoginForm({
      ...dataLoginForm,
      [e.target.name]: e.target.value
    })
  }

  const isFormValid = () => {
    if (email.trim().length === 0) {
      // dispatch(setError("Email es requerido"));
      alert.error('Email es requerido')

      return false
    } else if (!validator.isEmail(email)) {
      console.log('Email incorrecto')
      // dispatch(setError("Email incorrecto"));
      alert.error('Email incorrecto')
      return false
    } else if (password.length < 5) {
      alert.error('El password debe ser de al menos 5 caracteres')

      return false
    }

    // dispatch(setError(null));
    return true
  }

  const handleSubmitForm = async e => {
    e.preventDefault()
    if (isFormValid()) {
      let res = await axios
        .post(`${host}/login`, dataLoginForm)
        .catch(error => {
          alert.error(error.response.data.msg)
        })
      console.log(res)
      if (res && res.data.status === 'OK') {
        alert.success('Usuario Registrado')
        // console.log(res.data.token);
        const action = {
          type: types.login,
          payload: { uid: res.data.uid, name: res.data.name }
        }
        dispatch(action)

        // localStorage.setItem("token", res.data.token);
        // localStorage.setItem(
        //   "user",
        //   JSON.stringify({ name: res.data.name, uid: res.data.uid })
        // );
        // setisloggedIn(true);
      }
    }
  }
  return (
    <div className='login-max-container'>
      <div className='cont-form-login'>
        <h5 className='text-center mb-3'>INGRESE LOS DATOS REQUERIDOS</h5>
        <form onSubmit={handleSubmitForm}>
          <div className='items-form'>
            <label htmlFor='' className='label-email'>
              Email
            </label>
            <div className='input-email'>
              <input
                autoComplete='off'
                type='email'
                className=''
                id='staticEmail'
                placeholder='email@example.com'
                required
                name='email'
                onChange={onchangeForm}
              />
            </div>
          </div>
          <div className='password-class'>
            <label htmlFor='' className='label-password'>
              Password
            </label>
            <div className='input-password'>
              <input
                autoComplete='off'
                type={viewPassword ? 'text' : 'password'}
                className='password'
                id='inputPassword'
                placeholder='Password'
                required
                name='password'
                onChange={onchangeForm}
              />
              <button
                className='button-password'
                onClick={() => {
                  setviewPassword(!viewPassword)
                }}
                type='button'
              >
                {viewPassword ? (
                  <i className='fas fa-eye'></i>
                ) : (
                  <i className='fas fa-eye-slash'></i>
                )}
              </button>
            </div>
          </div>

          <button type='submit' className='button-primary'>
            INGRESAR <i className='fas fa-sign-in-alt'></i>
          </button>
          <div className='recuperacion-password'>
            <p
              onClick={() => {
                // history.push("/registro");
                // Router.push('/recuperacionPassword')
              }}
            >
              Olvidó su contraseña?
            </p>
          </div>
        </form>
        <div className='go-registro'>
          <p>Aun no esta registrado ?</p>

          <Link to='/auth/register' className='button-secondary'>
            Registrese
          </Link>

          {/* <button
            className="button-secondary"
            onClick={() => {
              console.log(" ddd");
              history.push("/register");

            }}
          >
            Registrate
          </button> */}
        </div>
      </div>
    </div>
  )
}

export default Login
