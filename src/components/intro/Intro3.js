import React from 'react'
import { useHistory } from 'react-router'

const Intro3 = () => {
  let history = useHistory()
  const goVideo = () => {
    history.push('videoScreen')
  }
  return (
    <div className='back-principal'>
      <div className='container intro2-container'>
        <div className='info'>
          <div className='image-mascota'>
            <img src='img/mascota/buho.png' alt='' />
          </div>
          <p>
            A continuación te vamos a presentar una historia que te contará
            sobre la materialización de un evento de riesgo operativo, luego que
            termine la historia podrás empezar a realizar la simulación de la
            gestión de riesgo operativo en función de la información aportada en
            la historia
          </p>
        </div>
        <div className='icons-intro'>
          <i className='fas fa-chevron-circle-right' onClick={goVideo}></i>
        </div>
      </div>
    </div>
  )
}

export default Intro3
