import React from 'react'
import { useHistory } from 'react-router-dom'


const Intro2 = () => {
  let history = useHistory()
  const goIntro3 = () => {
    history.push('intro3')
  }
 
  return (
    <div className='back-principal'>
      <div className='container intro2-container'>
        <div className='info'>
          <div className='image-mascota'>
            <img src='img/mascota/buho.png' alt='' />
          </div>
          <p>
            Es oportuno recordarte que la gestión de Riesgos es un proceso
            cíclico que consiste en identificar los factores, la tipología y las
            causas que generan dicha exposición, así como un análisis de los
            niveles de impacto y probabilidad con los que se presentan a fin de
            poder realizar una evaluación del nivel de la exposición y buscar el
            tratamiento adecuado para mitigar dicha exposición.
          </p>
        </div>
        <div className="icons-intro" onClick={goIntro3}>
        <i className='fas fa-chevron-circle-right'></i>
        </div>
      </div>
    </div>
  )
}

export default Intro2
