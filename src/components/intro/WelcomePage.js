import React from 'react'
import Navbar from '../Navbar'
import { useHistory } from 'react-router-dom'

// import moduleName from '../img/mascota/bu';
const WelcomePage = () => {
  let history = useHistory()
  const goIntro2 = () => {
    history.push('intro2')
  }
  return (
    <>
      <div className='back-principal'>

        <div className='title'>
          <h1>TE DAMOS LA BIENVENIDA A LA SIMULACIÓN N°1</h1>
          <h1>LA MISMA QUE TRATARÁ SOBRE EL TEMA DE FRAUDE INTERNO.</h1>
        </div>
        <div className='image-mascota'>
          <div className='circle'></div>
          <img src='img/mascota/buho.png' alt='' />
        </div>
        <i className='fas fa-chevron-circle-right' onClick={goIntro2}></i>
      
      </div>
    </>
  )
}

export default WelcomePage
