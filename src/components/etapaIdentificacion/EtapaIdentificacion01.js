import React from 'react'

const EtapaIdentificacion01 = () => {
  const botonesArr = [
    {
      title: 'PERSONAS',
      info:
        'La falla en las actividades desempeñadas por las personas, ya sea por la competencia, conducta ética o atribuciones que tenga un funcionario generan una exposición a riesgo operativo. Cuando un empleado accede  a transacciones que no son de su competencia, puede llegar a cambiar información sensible o tener a su disposición información confidenciales de los clientes o de la compañía, lo que puede resultar en fraudes, robos, sabotajes, etc.'
    },

    {
      title: 'PROCESOS',
      info:
        'La falla en los flujos, las etapas de desarrollo de productos o servicios, el registro de clientes, las transacciones que no han sido ingresadas de forma correcta en el sistema, registros incompletos con información imprecisa o fuera del periodo contable correspondiente, el uso del formato incorrecto en el ingreso de datos o el registro sin contraste con los datos existentes generan una exposición a riesgo operativo.'
    },
    {
      title: 'TECNOLOGÍA',
      info:
        'La falla en la captura, procesamiento, almacenamiento y transmisión de la información y servicios tecnológicos provistos por terceros generan una exposición a riesgo operativo.'
    },
    {
      title: 'EVENTOS EXTERNOS',
      info:
        'La materialización en eventos ajenos al control de la empresa tales como las fallas en los servicios públicos, los desastres naturales, atentados y actos delictivos así como las fallas en servicios críticos provistos por terceros'
    }
  ]
  return (
    <div className='back-principal'>
      <div className='container etapa-identificacion01'>
        <div className='title'>
          <h1>FACTOR DE RIESGO OPERATIVO</h1>
        </div>
        <div className='info'>
          <p>
            Existen 4 factores de riesgo que generan una exposición a un evento
            de riesgo operativo o una materialización de un evento de pérdida.
            Estos son:
          </p>
        </div>
        <div className='factores'>
          <ol>
            <li>Personas</li>
            <li>Procesos</li>
            <li>Personas</li>
            <li>Tecnología</li>
            <li>Eventos Externos</li>
          </ol>
        </div>
        <p>
          Si pones el cursor sobre cada uno de ellos tendrás una idea mas clara
          antes de elegir tu respuesta.
        </p>

        <div className='interaction-etapa'>
          <div className='image-mascota'>
            <img src='img/mascota/buho.png' alt='' />
          </div>
          <div className='area-buttons'>
            {botonesArr.map(butt => (
              <button className='button-info button-focus-tool-tip'>
                {butt.title}
                <div className='content-show'>{butt.info}</div>
              </button>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default EtapaIdentificacion01
