import React, { useContext } from "react";
import ReactPlayer from "react-player";
import { ModalContext } from "../context/modalContext";
import Navbar from "./Navbar";

const VideoScreen = () => {
  const { setmodalEtapa1 } = useContext(ModalContext);
  const handleEndVideo = () => {
    setmodalEtapa1(true);
  };
  return (
    <div className="video-screen-container">
      <ReactPlayer
        url="video/videoTest.mp4"
        width="100vw"
        height="100vh"
        controls={true}
        playing={true}
        onEnded={handleEndVideo}
      />
    </div>
  );
};

export default VideoScreen;
