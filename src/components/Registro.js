import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import { useAlert } from 'react-alert'
import validator from 'validator'
import axios from 'axios'
import { UntilContext } from '../context/untilContext'
import { UserContext } from '../context/userContext'
import { AuthContext } from '../reducers/AuthContext'
import { types } from '../types/types'

const Registro = () => {
  const { dispatch } = useContext(AuthContext)

  const alert = useAlert()
  const [viewPassword, setviewPassword] = useState(false)
  const [viewPasswordConfirm, setviewPasswordConfirm] = useState(false)
  const { host } = useContext(UntilContext)
  const { setisloggedIn } = useContext(UserContext)

  const [dataRegistroForm, setdataRegistroForm] = useState({
    name: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: ''
  })
  const { name, lastName, email, password, confirmPassword } = dataRegistroForm

  const onchangeForm = e => {
    setdataRegistroForm({
      ...dataRegistroForm,
      [e.target.name]: e.target.value
    })
  }

  const isFormValid = () => {
    if (name.trim().length === 0) {
      // console.log('Nombre es requerido')
      alert.error('Nombre es requerido')
      return false
    } else if (lastName.trim().length === 0) {
      // dispatch(setError("Apellido es requerido"));
      alert.error('Apellido es requerido')

      return false
    } else if (email.trim().length === 0) {
      // dispatch(setError("Email es requerido"));
      alert.error('Email es requerido')

      return false
    } else if (!validator.isEmail(email)) {
      console.log('Email incorrecto')
      // dispatch(setError("Email incorrecto"));
      alert.error('Email incorrecto')
      return false
    } else if (password.length < 5) {
      alert.error('password debe ser de al menos 5 caracteres')

      return false
    } else if (password !== confirmPassword) {
      alert.error('Los passwords no coinciden')
    }

    // dispatch(setError(null));
    return true
  }

  const handleSubmitForm = async e => {
    e.preventDefault()
    if (isFormValid()) {
      let res = await axios
        .post(`${host}/registro`, dataRegistroForm)
        .catch(error => {
          // console.log("response: ", error.response.data);
          alert.error(error.response.data.msg)
        })
      // console.log(res);
      if (res && res.data.status === 'OK') {
        alert.success('Usuario Registrado')
        // console.log(res.data.token);
        const action = {
          type: types.login,
          payload: { name, lastName, email, password }
        }
        dispatch(action)
      }
    }
  }
  return (
    <div className='login-max-container'>
      <div className='cont-form-register'>
        <h5 className='text-center mb-3'>REGISTRARSE</h5>

        <form onSubmit={handleSubmitForm}>
          <div className='items-form'>
            <label htmlFor='' className='label-email'>
              Nombre
            </label>
            <div className='input-email'>
              <input
                type='text'
                className=''
                id='name'
                placeholder='Nombre'
                required
                name='name'
                onChange={onchangeForm}
              />
            </div>
          </div>

          <div className='items-form'>
            <label htmlFor='' className='label-email'>
              Apellido
            </label>
            <div className='input-email'>
              <input
                type='text'
                className=''
                id='lastName'
                placeholder='Apellido'
                required
                name='lastName'
                onChange={onchangeForm}
              />
            </div>
          </div>

          <div className='items-form'>
            <label htmlFor='' className='label-email'>
              Email
            </label>
            <div className='input-email'>
              <input
                type='email'
                className=''
                id='staticEmail'
                placeholder='email@example.com'
                required
                name='email'
                onChange={onchangeForm}
              />
            </div>
          </div>
          <div className='items-form'>
            <label htmlFor='' className='label-password'>
              Password
            </label>
            <div className='input-password'>
              <input
                type={viewPassword ? 'text' : 'password'}
                className='password'
                id='inputPassword'
                placeholder='Password'
                required
                name='password'
                onChange={onchangeForm}
              />
              <button
                className='button-password'
                onClick={() => {
                  setviewPassword(!viewPassword)
                }}
                type='button'
              >
                {viewPassword ? (
                  <i className='fas fa-eye'></i>
                ) : (
                  <i className='fas fa-eye-slash'></i>
                )}
              </button>
            </div>
          </div>
          <div className='items-form'>
            <label htmlFor='' className='label-password'>
              Confirm Password
            </label>
            <div className='input-password'>
              <input
                type={viewPasswordConfirm ? 'text' : 'password'}
                className='password'
                id='inputPasswordConf'
                placeholder='Password'
                required
                name='confirmPassword'
                onChange={onchangeForm}
              />
              <button
                className='button-password'
                onClick={() => {
                  setviewPasswordConfirm(!viewPasswordConfirm)
                }}
                type='button'
              >
                {viewPasswordConfirm ? (
                  <i className='fas fa-eye'></i>
                ) : (
                  <i className='fas fa-eye-slash'></i>
                )}
              </button>
            </div>
          </div>

          <button type='submit' className='button-primary'>
            REGISTRARSE <i className='fas fa-sign-in-alt'></i>
          </button>
        </form>

        <div className='go-registro'>
          <p>Ya tiene una cuenta?</p>

          <Link to='/auth/login' className='button-secondary'>
            Inicie Sesion
          </Link>

          {/* <button
              className="button-secondary"
              onClick={() => {
                console.log(" ddd");
                history.push("/register");
  
              }}
            >
              Registrate
            </button> */}
        </div>
      </div>
    </div>
  )
}

export default Registro
