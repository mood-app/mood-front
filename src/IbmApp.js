import React, { useEffect, useReducer } from 'react'
import AppRouter from './routes/AppRouter'
import { Provider } from 'react-redux'
import { store } from './store/store'
import { AuthContext } from './reducers/AuthContext'
import { authReducer } from './reducers/authReducer'
const init = () => {
  return JSON.parse(localStorage.getItem('user')) || { logged: false }
}
const IbmApp = () => {
  const [user, dispatch] = useReducer(authReducer, {}, init)
  console.log(user)

  useEffect(() => {
    localStorage.setItem('user', JSON.stringify(user))
  }, [user])
  return (
    <AuthContext.Provider value={{ user, dispatch }}>
      <AppRouter />
    </AuthContext.Provider>
  )
}

export default IbmApp
