import React, { useContext } from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { UserContext } from '../context/userContext'
import AuthRouter from './AuthRouter'
import DashboardRoutes from './DashboardRoutes'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import ModalEtapa1 from '../components/ModalEtapa1'
import { AuthContext } from '../reducers/AuthContext'
import Navbar from '../components/Navbar'
const AppRouter = () => {
  const { user } = useContext(AuthContext)
  console.log(user)
  return (
    <Router>
      <ModalEtapa1 />
      <Navbar />

      <Switch>
        <PublicRoute
          isAuthenticated={user.logged}
          path='/auth'
          component={AuthRouter}
        />
        <PrivateRoute
          isAuthenticated={user.logged}
          path='/'
          component={DashboardRoutes}
        />
        {/* <Route path='/auth' component={AuthRouter} />
        <Route exact path='/' component={JournalScreen} />
        <Redirect to='/auth/login' /> */}
      </Switch>
    </Router>
  )
}

export default AppRouter
