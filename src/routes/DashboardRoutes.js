import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import WelcomePage from '../components/intro/WelcomePage'
import VideoScreen from '../components/VideoScreen'
import EtapaIdentificacion01 from '../components/etapaIdentificacion/EtapaIdentificacion01'
import Intro2 from '../components/intro/Intro2'
import Intro3 from '../components/intro/Intro3'
const DashboardRoutes = () => {
  return (
    <>
      <Switch>
        <Route exact path='/' component={WelcomePage} />
        <Route exact path='/videoScreen' component={VideoScreen} />
        <Route exact path='/intro2' component={Intro2} />
        <Route exact path='/intro3' component={Intro3} />
        <Route
          exact
          path='/etapaIdentificacion01'
          component={EtapaIdentificacion01}
        />

        <Redirect to='/' />
      </Switch>
    </>
  )
}

export default DashboardRoutes
