import React, {  } from 'react'
import { Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'

const PrivateRoute = ({ isAuthenticated, component: Component, ...rest }) => {
  // console.log(rest.location.pathname)

  //   localStorage.setItem('lastPath', rest.location.pathname)
  // console.log(isAuthenticated)
   return (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? <Component {...props} /> : <Redirect to='/auth/login' />
    }
  />
  )
  // return null
}

export default PrivateRoute

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired
}
