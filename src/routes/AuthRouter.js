import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Login from '../components/Login'
import Registro from '../components/Registro'

const AuthRouter = () => {
  return (
        <Switch>
          <Route exact path='/auth/login' component={Login} />
          <Route exact path='/auth/register' component={Registro} />
          <Redirect to='/auth/login' />
        </Switch>
  )
}

export default AuthRouter
