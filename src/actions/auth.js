import { types } from '../types/types'
import axios from 'axios'
import { useContext } from 'react'
import { UntilContext } from '../context/untilContext'
export const startLogin = async (dataRegistroForm, alert, host) => {
  let res = await axios
    .post(`${host}/registro`, dataRegistroForm)
    .catch(error => {
      // console.log("response: ", error.response.data);
      alert.error(error.response.data.msg)
    })
  // console.log(res);
  if (res && res.data.status === 'OK') {
    alert.success('Usuario Registrado')
    // console.log(res.data.token);
  }
  // return{
  //     type: types.login,
  //     payload: {
  //         uid, displayName
  //     }
  // }
}
