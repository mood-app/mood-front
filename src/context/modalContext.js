import React, { createContext, useState } from "react";

export const ModalContext = createContext();

const ModalProvider = (props) => {
  const [modalEtapa1, setmodalEtapa1] = useState(false);

  return (
    <ModalContext.Provider
      value={{
        modalEtapa1,
        setmodalEtapa1,
      }}
    >
      {props.children}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
