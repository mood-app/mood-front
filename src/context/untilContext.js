import React, { createContext } from 'react'

export const UntilContext = createContext()

const UntilProvider = props => {
  const host = 'http://127.0.0.1:5000'

  return (
    <UntilContext.Provider
      value={{
        host
      }}
    >
      {props.children}
    </UntilContext.Provider>
  )
}

export default UntilProvider
