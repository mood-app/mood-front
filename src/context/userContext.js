import React, { createContext, useState } from "react";

export const UserContext = createContext();

const UserProvider = (props) => {
  const [isloggedIn, setisloggedIn] = useState(false);
    

  return (
    <UserContext.Provider
      value={{
        isloggedIn,
        setisloggedIn,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export default UserProvider;
